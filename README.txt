
What
----
The PicLens module for Drupal makes it easy for you to provide your visitors 
with an immersive slideshow experience for rich media on your website. It supports
the media RSS feeds which is needed by the 3D plugins. The PicLens Lite is also supported
enabling a really slick slideshow on your own website.

See the PicLens Lite in action on http://realize.be/image-galleries/drupalcon-boston-2008

For more information about PicLens and browser 3D plugins, go to http://www.piclens.com/

Supported Modules
-----------------
The image gallery module is supported out of the box. It automatically creates a media rss feed 
listing all images in an image gallery. It can also add a 'Start slideshow' link on image 
gallery pages  to start PicLens Lite, a really slick slideshow interface. 
Go to http://expample.com/admin/settings/piclens for settings.

API
---
Several functions are available to create your own PicLens rss feed.

* piclens_feed_url($url, $title);
  This function adds the rss feed with the $url to your own menu callback providing the feed
  into the <head> tag of your document. $title is optional.

* piclens_lite_javascript()
  This function adds the javascript for the PicLens Lite into content region of your document.

* piclens_lite_link($drupal_set_message)
  This function returns a link to start the PicLens Lite slideshow.
  If $drupal_set_message = TRUE, the link will be inserted as a drupal message, otherwhise
  the link is simply returned. Inside this function theme('piclens_lite_html_link'); is called
  to return the html. You can override the theming with phptemplate_piclens_lite_html_link() off course. 

* piclens_format_item($item);
  This function formats one item (image, video) into a valid xml structure for the piclens feed.
  $item is an array with following properties:
  - title : title image or video
  - link : direct link to content
  - thumbnail : thumbnail url image or video
  - content : content url of image or video  

* piclens_rss($items);
  This function outputs the feed. $items is a string formatted xml list of $items.
  
/**
 * Example menu callback spitting out piclens rss feed with your own items.
 */
function example_module() {
  $items = '';
  // get all my items
  $my_items = db_query("SELECT * FROM {my_table}");
  while ($my_item = db_fetch_object($my_items)) {
    $item = array(
      'title' => $my_item->title,
      'link' => $my_item->link,
      'thumbnail' => $my_item->thumbnail,
      'content' => $my_item->content,
    );
    $items .= piclens_format_item($item);
  }
  // output rss feed. Exit is not needed, piclens_rss sets the appropiate header.
  piclens_rss($items);
}  

Bug reports, feature requests etc
---------------------------------
http://drupal.org/project/piclens
